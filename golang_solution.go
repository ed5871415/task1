package main

import (
    "fmt"
    "io/ioutil"
    "sort"
)

func main() {
    dir1 := "my_dir_1"
    dir2 := "my_dir_2"

    files := make(map[string]struct{})

    dirs := []string{dir1, dir2}

    for _, dir := range dirs {
        fileList, err := ioutil.ReadDir(dir)
        if err != nil {
            fmt.Printf("Error reading directory %s: %v\n", dir, err)
            continue
        }
        for _, file := range fileList {
            if file.Mode().IsRegular() {
                files[file.Name()] = struct{}{}
            }
        }
    }

    var fileList []string
    for file := range files {
        fileList = append(fileList, file)
    }
    sort.Strings(fileList)

    for _, file := range fileList {
        fmt.Println(file)
    }
}
