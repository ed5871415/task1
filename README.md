# Test1



## Завдання:
### За допомогою bash команди чи bash скрипту вирішити наступну задачу (додаткова реалізація за допомогою python чи за допомогою іншої мови програмування буде плюсом): необхідно вивести всі назви файлів з двох директорій так щоб вони були унікальними (Врахувати негативні кейси). У разі необхідності попередніх налаштувань, надати step by step інструкцію.

```
Приклад:
Наявна директорія my_dir_1. З наступним контентом:
■	my_dir_1
    ■	file1.txt
    ■	file2.txt
    ■	file3.txt
Наявна директорія my_dir_2. З наступним контентом:
■	my_dir_2
    ■	file2.txt
    ■	file4.txt

Бажаний результат:
file1.txt
file2.txt
file3.txt
file4.txt

```

### Для створення директорій можна скористуватися скриптом create_directories.sh, який створить вищезгадану структуру директорій та файли в них.

## Результати роботи скриптів можна побачити у Gitlab Pipeline 

![Pipeline's screenshot](/img/ED_task1_pipeline.jpg)

## Посилання на Pipeline
[Pipeline](https://gitlab.com/ed5871415/task1/-/pipelines/1252345728)

## Реалізації та команди запуску

### Реалізація на Bash 
Файл - bash_solution.sh

Запуск
```
bash bash_solution.sh
```

### Реалізація на Python
Файл - python_solution.py

Запуск
```
python3 python_solution.py
```

### Реалізація на GoLang
Файл - golang_solution.go

Запуск
```
go run golang_solution.go
```

### Реалізація на PHP
Файл - php_solution.php

Запуск
```
go run php_solution.php
```

