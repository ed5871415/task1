#!/bin/bash

dir1="my_dir_1"
dir2="my_dir_2"

files=$(find "$dir1" "$dir2" -type f -printf "%f\n" | sort -u)

printf "%s\n" "$files"