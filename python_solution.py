import os
from pathlib import Path

dir1 = "my_dir_1"
dir2 = "my_dir_2"

files = set()

for dir_path in (dir1, dir2):
    for file in os.listdir(dir_path):
        file_path = Path(dir_path) / file
        if file_path.is_file():
            files.add(file)

for file in sorted(files):
    print(file)