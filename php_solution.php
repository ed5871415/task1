<?php

$dir1 = "my_dir_1";
$dir2 = "my_dir_2";

$files = array();

foreach ([$dir1, $dir2] as $dir) {
    $fileList = scandir($dir);
    if ($fileList === false) {
        echo "Error reading directory $dir\n";
        continue;
    }
    foreach ($fileList as $file) {
        $filePath = $dir . DIRECTORY_SEPARATOR . $file;
        if (is_file($filePath)) {
            $files[$file] = true;
        }
    }
}

ksort($files);

foreach ($files as $file => $_) {
    echo $file . "\n";
}
?>
