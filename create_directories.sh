#!/bin/bash

# Создаем директорию my_dir_1
mkdir -p my_dir_1

# Создаем файлы в директории my_dir_1
touch my_dir_1/file1.txt
touch my_dir_1/file2.txt
touch my_dir_1/file3.txt

# Создаем директорию my_dir_2
mkdir -p my_dir_2

# Создаем файлы в директории my_dir_2
touch my_dir_2/file2.txt
touch my_dir_2/file4.txt
